require_relative "../lib/lab42/forwarder3"
require "benchmark"

class Bm1
  extend Lab42::Forwarder3


  def_delegator :@hash, :[], :fetch1
  forward :fetch3, to: :@hash, as: :[]


  def initialize
    @hash = Hash[("a".."z").zip(1..26)]
  end
end

N = ARGV.fetch(0, 100_000).to_i

bm1 = Bm1.new

p bm1.fetch1("q")
p bm1.fetch3("q")

Benchmark.bmbm do |bm|
  bm.report("forwarder3") {N.times{bm1.fetch3("q")}}
  bm.report("forwardable") {N.times{bm1.fetch1("q")}}
end
