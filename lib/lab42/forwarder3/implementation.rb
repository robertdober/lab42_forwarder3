module Lab42
  module Forwarder3
    module Implementation extend self
      def define_forwarder(mthd_name, to:, as:, receiver:)
        receiver.def_delegator to, as, mthd_name 
      end

      private

    end
  end
end
