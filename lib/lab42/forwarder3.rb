require_relative "forwarder3/implementation"
require "forwardable"
module Lab42
  module Forwarder3
    def self.extended by
      by.extend Forwardable
    end

    extend Forwardable
    def forward(mthd_name, to:, as: nil)
      as ||= mthd_name
      Implementation.define_forwarder(mthd_name, to: to, as: as, receiver: self)
    end

    def forward_all(*mthd_names, to:)
      mthd_names.each do |mthd_name|
        forward mthd_name, to: to
      end
    end
  end
end
