$:.unshift( File.expand_path( "../lib", __FILE__ ) )
require "lab42/forwarder3/version"
version = Lab42::Forwarder3::VERSION
Gem::Specification.new do |s|
  s.name        = 'lab42_forwarder3'
  s.version     = version
  s.summary     = 'Forwarder3 a readable zero cost API around Forwardable'
  s.description = %{Forwarder3 a readable and zero cost API around Forwardable}
  s.authors     = ["Robert Dober"]
  s.email       = 'robert.dober@gmail.com'
  s.files       = Dir.glob("lib/**/*.rb")
  s.files      += %w{LICENSE README.md}
  s.homepage    = "https://bitbucket.org/robertdober/lab42_forwarder3"
  s.licenses    = %w{Apache-2.0}

  s.required_ruby_version = '>= 2.7.0'
end
